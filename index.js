const express = require("express");

const mongoose = require("mongoose");

const app=express();
const port=3000;

// Section - MongoB Connection
/*Synarx:
	mongoose.connect("<MongoDB atlas connection string>", {useNewUrlParser})

*/
// Connect to the data by passing in your connection string (from MongoDB)
// ".connect()" lets us connect and access teh database that is sent to it via string
// "b190-to-do" is the database that we have created in our MongoDB - need mo kasi ng database kung san mo istore 
// yung useNewUrlParser and unifiedTopology naman - prevents Mongoose from being used in application. pag wala ka nito, connected ka pa din sa database pero kung magsend ka ng request, unnecessary warnings may be sent too

mongoose.connect("mongodb+srv://jpunzalan:iloveNZ@wdc028-course-booking.0tib1.mongodb.net/b190-to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// sets notifications for connection success or failure
// allows us to handle errors when initial connection is established
// works with   on and once mongoose methods
let db = mongoose.connection;
// if a connection error occured, we will be seeing it in the cnsole
// console.error.bind allows us to prit errors in the browser as well as in the terminal (para makita natin yung mga errors)
db.on("error", console.error.bind(console, "connection error"));
// if teh connection is successful, confirm it in the console. para siyang app.listen to check kung running yung server. pwede kasing running yung serer natin pero nagkaroon ng connection error:
db.once("open", ()=>console.log("we're connected to the database"));

// Schema() is a method inside the mongoose module that lets us create schema for our database it receives an object with properties and data types that each property should have
const taskSchema=new mongoose.Schema({
	name: String, 
	// the name property should receive a string data type(dapat capital letter un umpisa otherwise hindi mababasa (Sentence case))
	status: {
		type: String,
		// ang default property naman allows the server to automatically assign a value to the property once the user fails to provide one
		default: "pending"
	}
});

// Section - Models
// model in mongoose use schemas and are used to complete the object instantiation that correspond to that schema
// Models use Schemas and they act as the middleman from the server to the database
// First parameter is the collection in where to store the data 
// second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection
// Models must be written in singular form, sentence case
// The "Task" variable now will be used for us to run commands for interacting with our database

const Task = mongoose.model("Task", taskSchema);

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Create a new task
/*
Business Logig:
	- add a functionality that will check if there are duplicate tasks
		- if teh task is already existing, we return an error
		- if the task is not existing, we add it in the database
	- the task will be sent from the request body
	-create a new Task object with a "name" field/property
	- the "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object

*/

app.post("/tasks", (req,res)=>{
	// findOne is the mongoose method for finding documents in the database. It works similar to ".find()" in MongoDB"
	// findOne returns the first document it finds, if there are any; if there are no documents, it will return "null"
	 // 
	Task.findOne({ name : req.body.name}, (err, result)=>{
		if(result!==null&&result.name===req.body.name){
			return res.send("Duplicate task found");
		}else{
			let newTask=new Task({
				name: req.body.name
			}); 
			newTask.save((saveErr, savedTask)=>{
				if(saveErr){
					return console.error(saveErr);
				}else{
					return res.status(201).send("New task created")
				};
			});
		};
	});
});


// Mini activity
/*
getting all the tasks
Business Logic:
- retrieve all documents
- if there are errors, print the error
- if there are none, send a success status back and return the array of documents

*/
app.get("/tasks",(req, res)=>{

	Task.find({ }, (err, result)=>{
		if(err){
			return console.log(err)}else{
			return res.status(200).json({
				data: result
			});
		};
	});
});

// app.delete("/tasks", (req, res) => {
//    res.send(`task has been deleted.`);
// });


// other solution
// app.get("/tasks",(req, res)=>{

// 	Task.find({ }, (err, result)=>{
// 		if(result===null){
// 			return console.log("Error")}else{
// 			return res(result);
// 		};
// 	});
// });


// ACTIVITY
/*
Business Logic for registering a new user:
	- add a functionality to check if there are duplicate users
		- if the user is already existing, return an error
		- if the user is not existing, save /add the user in the database
	- the user will be coming from the request's body
	- create a new User object with a "username" field/property
	- create a new User object with a "username" and "password" fields/properties 
*/

/*
1. Create a User schema.
2. Create a User model.
3. Create a POST route that will access the "/signup" route that will create a user.
4. Process a POST request at the "/signup" route using postman to register a user.
5. Create a git repository named S35.
6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
7. Add the link in Boodle.
*/


const userSchema=new mongoose.Schema({
	username: String, 
	status: {
		type: String,
		default: "pending"
	}
});

const User = mongoose.model("User", userSchema);

app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.post("/signup", (req,res)=>{
	 
	User.findOne({ username : req.body.username}, (err, result)=>{
		if(result!==null&&result.username===req.body.username){
			return res.send("User already existing");
		}else{
			let newUser=new User({
				username: req.body.username
			}); 
			newUser.save((saveErr, savedUser)=>{
				if(saveErr){
					return console.error(saveErr);
				}else{
					return res.status(201).send("New user registered")
				};
			});
		};
	});
});

app.listen(port, ()=> console.log(`Server running at port: ${port}`));

